***The most exciting phrase to hear in science, the  
one that heralds new discoveries, is not “Eureka!”  
(I found it!) but rather, “hmm.... that’s funny...”***  
*— Isaac Asimov.*

![](./images/intro.png)

What its all about
==================

This repository contain usefull material for developing a solid machine and a sum of useful skills.  

  
Note: A repository intend to live and evolve, please do add our input to it and we will get an all in one folder of the project.
That would be great!


Guide
-----

Here goes all the informations that are related to the machine utilisation. 

Biblio
------

This part is pretty static, create a folder with your name if you want to add stuffs.
In the biblio folder, please do find a diverese poll of paper and books, that I hope can offer you some useful material.


Shopping
--------

The shopping folder is about the list of material used during the project.
We will do a Latex file for that