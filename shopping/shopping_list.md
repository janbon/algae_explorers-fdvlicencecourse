

What its all about
==================

Here goes the shopping list to construct a setup for optogenetic.

I divide it into 3 section: required, I have, optionnal.


required
--------
  
+ [A raspicam v2](https://www.raspberrypi.org/products/camera-module-v2/)
  
+ Some lenses (both would be cool so we can play with optics) [1 biconvex lens](https://www.edmundoptics.eu/p/25mm-dia-x-50mm-fl-uncoated-double-convex-lens/2460/) or [2 PCX lens](https://www.edmundoptics.eu/p/250mm-dia-x-500mm-fl-uncoated-plano-convex-lens/2362/)
  
+ [2 Raspberry + SD cards (16 or 32 Go)](https://www.raspberrypi.org/products/raspberry-pi-3-model-b-plus/)
(or 1 Raspberry of the new model with 4Go of RAM)

+ [1 BeagleBone + SD card](https://fr.farnell.com/beagleboard/bbone-black-4g/beaglebone-black-rev-c-cortec/dp/2422228?gclid=Cj0KCQjwjOrtBRCcARIsAEq4rW6P_WqFFvJbcLLmf4JTxwVHR1RQEYI3fJtBkKTqDzlLL_cZQQSR9pYaAgv3EALw_wcB&gross_price=true&mckv=pY3iAeca_dc|pcrid|80994017342|&CMP=KNC-GFR-GEN-SHOPPING-2422228)
  
+ Some PLA filament (1 or 2 Kg)
  
+ microscope glasse slides to make a chamber (2 by 2 cmm), I don't have much of these could be good to make them construct a small chamber from two microscope slides.

+ 5v fan

+ [DHT22 temperature sensor](
https://fr.farnell.com/mcm/83-17985/dht22-temp-humidity-sensor/dp/2801405?CMP=KNC-GFR-GEN-KWL-FEED-SKU&mckv=_dc|pcrid|297375359740|&gclid=Cj0KCQjwjOrtBRCcARIsAEq4rW7xPNGTCUX9pPGnM61m9pTkOtuz_WM77lgowDmotTKgl-Foowfud-waArsTEALw_wcB)

I have
------
  
+ [A projector DLP® LightCrafter™ Display 2000](http://www.ti.com/tool/DLPDLCR2000EVM)
  
+ Cables

+ Petri dish and growth media

optional
--------

+ [A raspicam Pinoir v2](https://www.raspberrypi.org/products/pi-noir-camera-v2/)

+ Scavenge an old computer alim to power the setup





About these yeasts
==================


[A raspicam Pinoir v2](https://www.raspberrypi.org/products/pi-noir-camera-v2/)